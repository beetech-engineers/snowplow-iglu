-- Copyright (c) 2014 Snowplow Analytics Ltd. All rights reserved.
--
-- This program is licensed to you under the Apache License Version 2.0,
-- and you may not use this file except in compliance with the Apache License Version 2.0.
-- You may obtain a copy of the Apache License Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0.
--
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the Apache License Version 2.0 is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Apache License Version 2.0 for the specific language governing permissions and limitations there under.
--
-- Authors:     Gustavo Hwu Lee
--

CREATE TABLE atomic.com_hubspot_snowplow_email_events_updates_1
(
  -- Schema of this type
  schema_vendor         varchar(128) encode runlength not null,
  schema_name           varchar(128) encode runlength not null,
  schema_format         varchar(128) encode runlength not null,
  schema_version        varchar(128) encode runlength not null,
  -- Parentage of this type
  root_id               char(36) encode raw not null,
  root_tstamp           timestamp encode raw not null,
  ref_root              varchar(255) encode runlength not null,
  ref_tree              varchar(1500) encode runlength not null,
  ref_parent            varchar(255) encode runlength not null,
  -- Properties of this type
  appName               varchar(255),
  ipAddress             varchar(255),
  "browser.name"        varchar(255),
  "browser.family"      varchar(255),
  "browser.producer"    varchar(255),
  "browser.producerUrl" varchar(255),
  "browser.type"        varchar(255),
  "browser.url"         varchar(255),
  referer               varchar(255),
  linkId                bigint,
  url                   varchar(255),
  userAgent             varchar(255),
  appId                 bigint,
  created               varchar(255),
  "location.country"    varchar(255),
  "location.state"      varchar(255),
  "location.city"       varchar(255),
  id                    varchar(255),
  recipient             varchar(255),
  "from"                  varchar(1000),
  subject               varchar(1000),
  "sentBy.id"           varchar(255),
  portalId              bigint,
  type                  varchar(255),
  filteredEvent         boolean,
  deviceType            varchar(255),
  emailCampaignId       bigint,

  FOREIGN KEY (root_id) REFERENCES atomic.events (event_id)
)
  DISTSTYLE KEY
  -- Optimized join to atomic.events
  DISTKEY
(
  root_id
)
  SORTKEY
(
  root_tstamp
);
