-- Copyright (c) 2014 Snowplow Analytics Ltd. All rights reserved.
--
-- This program is licensed to you under the Apache License Version 2.0,
-- and you may not use this file except in compliance with the Apache License Version 2.0.
-- You may obtain a copy of the Apache License Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0.
--
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the Apache License Version 2.0 is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Apache License Version 2.0 for the specific language governing permissions and limitations there under.
--
-- Authors:     Gustavo Hwu Lee
--

CREATE TABLE atomic.com_pipedrive_snowplow_stage_updates_1
(
  -- Schema of this type
  schema_vendor  varchar(128) encode runlength not null,
  schema_name    varchar(128) encode runlength not null,
  schema_format  varchar(128) encode runlength not null,
  schema_version varchar(128) encode runlength not null,
  -- Parentage of this type
  root_id        char(36) encode raw not null,
  root_tstamp    timestamp encode raw not null,
  ref_root       varchar(255) encode runlength not null,
  ref_tree       varchar(1500) encode runlength not null,
  ref_parent     varchar(255) encode runlength not null,
  -- Properties of this type
  v bigint,
"meta.v" bigint,
"meta.action" varchar(255),
"meta.object" varchar(255),
"meta.id" bigint,
"meta.company_id" bigint,
"meta.user_id" bigint,
"meta.host" varchar(255),
"meta.timestamp" bigint,
"meta.timestamp_micro" bigint,
"meta.trans_pending" boolean,
"meta.is_bulk_update" boolean,
"meta.webhook_id" varchar(255),
"current.id" bigint,
"current.order_nr" bigint,
"current.name" varchar(255),
"current.active_flag" boolean,
"current.deal_probability" bigint,
"current.pipeline_id" bigint,
"current.rotten_flag" boolean,
"current.rotten_days" bigint,
"current.add_time" varchar(255),
"current.update_time" varchar(255),
"previous.id" bigint,
"previous.order_nr" bigint,
"previous.name" varchar(255),
"previous.active_flag" boolean,
"previous.deal_probability" bigint,
"previous.pipeline_id" bigint,
"previous.rotten_flag" boolean,
"previous.rotten_days" bigint,
"previous.add_time" varchar(255),
"previous.update_time" varchar(255),
event varchar(255),
retry bigint,

    FOREIGN KEY (root_id) REFERENCES atomic.events(event_id)
)
  DISTSTYLE KEY
  -- Optimized join to atomic.events
  DISTKEY
(
  root_id
)
  SORTKEY
(
  root_tstamp
);
