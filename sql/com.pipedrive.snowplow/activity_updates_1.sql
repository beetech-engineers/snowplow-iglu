-- Copyright (c) 2014 Snowplow Analytics Ltd. All rights reserved.
--
-- This program is licensed to you under the Apache License Version 2.0,
-- and you may not use this file except in compliance with the Apache License Version 2.0.
-- You may obtain a copy of the Apache License Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0.
--
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the Apache License Version 2.0 is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Apache License Version 2.0 for the specific language governing permissions and limitations there under.
--
-- Authors:     Gustavo Hwu Lee
--

CREATE TABLE atomic.com_pipedrive_snowplow_activity_updates_1
(
  -- Schema of this type
  schema_vendor                          varchar(128) encode runlength not null,
  schema_name                            varchar(128) encode runlength not null,
  schema_format                          varchar(128) encode runlength not null,
  schema_version                         varchar(128) encode runlength not null,
  -- Parentage of this type
  root_id                                char(36) encode raw not null,
  root_tstamp                            timestamp encode raw not null,
  ref_root                               varchar(255) encode runlength not null,
  ref_tree                               varchar(1500) encode runlength not null,
  ref_parent                             varchar(255) encode runlength not null,
  -- Properties of this type
  v                                      bigint,
  "meta.v"                               bigint,
  "meta.action"                          varchar(255),
  "meta.object"                          varchar(255),
  "meta.id"                              bigint,
  "meta.company_id"                      bigint,
  "meta.user_id"                         bigint,
  "meta.host"                            varchar(255),
  "meta.timestamp"                       bigint,
  "meta.timestamp_micro"                 bigint,
  "meta.trans_pending"                   boolean,
  "meta.is_bulk_update"                  boolean,
  "meta.webhook_id"                      varchar(255),
  "current.id"                           bigint,
  "current.company_id"                   bigint,
  "current.user_id"                      bigint,
  "current.done"                         boolean,
  "current.type"                         varchar(255),
  "current.due_date"                     varchar(255),
  "current.due_time"                     varchar(255),
  "current.duration"                     varchar(255),
  "current.add_time"                     varchar(255),
  "current.marked_as_done_time"          varchar(255),
  "current.last_notification_time"       varchar(255),
  "current.last_notification_user_id"    bigint,
  "current.notification_language_id"     bigint,
  "current.subject"                      varchar(255),
  "current.org_id"                       bigint,
  "current.person_id"                    bigint,
  "current.deal_id"                      bigint,
  "current.active_flag"                  boolean,
  "current.update_time"                  varchar(255),
  "current.update_user_id"               bigint,
  "current.source_timezone"              varchar(255),
  "current.note"                         varchar(255),
  "current.created_by_user_id"           bigint,
  "current.org_name"                     varchar(255),
  "current.person_name"                  varchar(255),
  "current.deal_title"                   varchar(255),
  "current.owner_name"                   varchar(255),
  "current.person_dropbox_bcc"           varchar(255),
  "current.deal_dropbox_bcc"             varchar(255),
  "current.assigned_to_user_id"          bigint,
  "previous.id"                          bigint,
  "previous.company_id"                  bigint,
  "previous.user_id"                     bigint,
  "previous.done"                        boolean,
  "previous.type"                        varchar(255),
  "previous.due_date"                    varchar(255),
  "previous.due_time"                    varchar(255),
  "previous.duration"                    varchar(255),
  "previous.add_time"                    varchar(255),
  "previous.marked_as_done_time"         varchar(255),
  "previous.last_notification_time"      varchar(255),
  "previous.last_notification_user_id"   bigint,
  "previous.notification_language_id"    bigint,
  "previous.subject"                     varchar(255),
  "previous.org_id"                      bigint,
  "previous.person_id"                   bigint,
  "previous.deal_id"                     bigint,
  "previous.active_flag"                 boolean,
  "previous.update_time"                 varchar(255),
  "previous.update_user_id"              bigint,
  "previous.source_timezone"             varchar(255),
  "previous.note"                        varchar(255),
  "previous.created_by_user_id"          bigint,
  "previous.org_name"                    varchar(255),
  "previous.person_name"                 varchar(255),
  "previous.deal_title"                  varchar(255),
  "previous.owner_name"                  varchar(255),
  "previous.person_dropbox_bcc"          varchar(255),
  "previous.deal_dropbox_bcc"            varchar(255),
  "previous.assigned_to_user_id"         bigint,
  event                                  varchar(255),
  retry                                  bigint,

  FOREIGN KEY (root_id) REFERENCES atomic.events (event_id)
)
  DISTSTYLE KEY
  -- Optimized join to atomic.events
  DISTKEY
(
  root_id
)
  SORTKEY
(
  root_tstamp
);
