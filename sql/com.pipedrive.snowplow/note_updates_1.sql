-- Copyright (c) 2014 Snowplow Analytics Ltd. All rights reserved.
--
-- This program is licensed to you under the Apache License Version 2.0,
-- and you may not use this file except in compliance with the Apache License Version 2.0.
-- You may obtain a copy of the Apache License Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0.
--
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the Apache License Version 2.0 is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Apache License Version 2.0 for the specific language governing permissions and limitations there under.
--
-- Authors:     Gustavo Hwu Lee
--

CREATE TABLE atomic.com_pipedrive_snowplow_note_updates_1
(
  -- Schema of this type
  schema_vendor                          varchar(128) encode runlength not null,
  schema_name                            varchar(128) encode runlength not null,
  schema_format                          varchar(128) encode runlength not null,
  schema_version                         varchar(128) encode runlength not null,
  -- Parentage of this type
  root_id                                char(36) encode raw not null,
  root_tstamp                            timestamp encode raw not null,
  ref_root                               varchar(255) encode runlength not null,
  ref_tree                               varchar(1500) encode runlength not null,
  ref_parent                             varchar(255) encode runlength not null,
  -- Properties of this type
  v                                      bigint,
  "meta.v"                               bigint,
  "meta.action"                          varchar(255),
  "meta.object"                          varchar(255),
  "meta.id"                              bigint,
  "meta.company_id"                      bigint,
  "meta.user_id"                         bigint,
  "meta.host"                            varchar(255),
  "meta.timestamp"                       bigint,
  "meta.timestamp_micro"                 bigint,
  "meta.trans_pending"                   boolean,
  "meta.is_bulk_update"                  boolean,
  "meta.webhook_id"                      varchar(255),
  "current.id"                           bigint,
  "current.user_id"                      bigint,
  "current.deal_id"                      bigint,
  "current.person_id"                    bigint,
  "current.org_id"                       bigint,
  "current.content"                      varchar(10000),
  "current.add_time"                     varchar(255),
  "current.update_time"                  varchar(255),
  "current.active_flag"                  boolean,
  "current.pinned_to_deal_flag"          boolean,
  "current.pinned_to_person_flag"        boolean,
  "current.pinned_to_organization_flag"  boolean,
  "current.last_update_user_id"          bigint,
  "current.organization.name"            varchar(255),
  "current.person.name"                  varchar(255),
  "current.deal.title"                   varchar(500),
  "current.user.email"                   varchar(500),
  "current.user.name"                    varchar(500),
  "current.user.icon_url"                varchar(1000),
  "current.user.is_you"                  boolean,
  "previous.id"                          bigint,
  "previous.user_id"                     bigint,
  "previous.deal_id"                     bigint,
  "previous.person_id"                   bigint,
  "previous.org_id"                      bigint,
  "previous.content"                     varchar(10000),
  "previous.add_time"                    varchar(255),
  "previous.update_time"                 varchar(255),
  "previous.active_flag"                 boolean,
  "previous.pinned_to_deal_flag"         boolean,
  "previous.pinned_to_person_flag"       boolean,
  "previous.pinned_to_organization_flag" boolean,
  "previous.last_update_user_id"         bigint,
  "previous.organization.name"           varchar(255),
  "previous.person.name"                 varchar(255),
  "previous.deal.title"                  varchar(500),
  "previous.user.email"                  varchar(500),
  "previous.user.name"                   varchar(500),
  "previous.user.icon_url"               varchar(1000),
  "previous.user.is_you"                 boolean,
  event                                  varchar(255),
  retry                                  bigint,

  FOREIGN KEY (root_id) REFERENCES atomic.events (event_id)
)
  DISTSTYLE KEY
  -- Optimized join to atomic.events
  DISTKEY
(
  root_id
)
  SORTKEY
(
  root_tstamp
);
