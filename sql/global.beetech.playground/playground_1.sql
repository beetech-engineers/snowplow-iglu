create table global_beetech_growth_playground_1
(
	schema_vendor varchar(128) not null encode runlength,
	schema_name varchar(128) not null encode runlength,
	schema_format varchar(128) not null encode runlength,
	schema_version varchar(128) not null encode runlength,
	root_id char(36) not null distkey
		constraint global_beetech_growth_playground_1_root_id_fkey
			references events,
	root_tstamp timestamp not null,
	ref_root varchar(255) not null encode runlength,
	ref_tree varchar(1500) not null encode runlength,
	ref_parent varchar(255) not null encode runlength,
	customer_id bigint not null,
	old_responsible_operator varchar(255),
	new_responsible_operator varchar(255),
	updated_at timestamp not null,
	reason varchar(255)
)
diststyle key
sortkey(root_tstamp)
;

alter table global_beetech_growth_playground_1 owner to beemaster;

