import json

x = """
{
    "appName": "Batch",
    "ipAddress": "179.191.124.250",
    "browser": {
        "name": "Chrome 72.0.3626.121",
        "family": "Chrome",
        "producer": "Google Inc.",
        "producerUrl": "https://www.google.com/about/company/",
        "type": "Browser",
        "url": "http://www.google.com/chrome/",
        "version": [
            "72.0.3626.121"
        ]
    },
    "referer": "",
    "linkId": 0,
    "url": "https://cta-image-cms2.hubspot.com/ctas/v2/public/cs/ci/?pg=9ae72a29-ea01-414b-8820-b969e1e76570&pid=3381861&ecid=ACsprvusQnUSyumHADMXHWPvWwyY23u4zyijSm1jHzDQjhcoOoDkAQnTkSUMw2oD4hIISm4Mo-7_&hseid=70972734&hsic=false&utm_rewrite=REWRITE_BARE&utm_campaign=RM_DCBE&utm_source=hs_email&utm_medium=email&utm_content=70972734",
    "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "appId": 113,
    "created": 1553108056807,
    "location": {
        "country": "BRAZIL",
        "state": "sao paulo",
        "city": "sao paulo",
        "latitude": -23.5475
    },
    "id": "710312d1-4175-3a5c-a10c-fec842ee1383",
    "recipient": "giovani@beetech.global",
    "sentBy": {
        "id": "5cf043a0-8b9e-40b8-8978-a045aa03c8a6",
        "created": 1553107238708
    },
    "portalId": 3381861,
    "type": "CLICK",
    "filteredEvent": false,
    "deviceType": "COMPUTER",
    "emailCampaignId": 70972734
}
"""


def parse(data, path = ''):
    json_paths = []
    sql_fields = ""
    o = {
        "type": ["object", "null"],
        "properties": {

        },
        "additionalProperties": True
    }
    for key, value in data.items():
        if type(value) == int:
            if path == '':
                sql_fields += "{} bigint,\n".format(key)
                json_paths.append("{}".format(key))
            else:
                sql_fields += "\"{}.{}\" bigint,\n".format(path, key)
                json_paths.append("{}.{}".format(path, key))


            o['properties'][key] = {
                "type": ["integer", "null"]
            }
        elif type(value) == str:
            if path == '':
                sql_fields += "{} varchar(255),\n".format(key)
                json_paths.append("{}".format(key))
            else:
                sql_fields += "\"{}.{}\" varchar(255),\n".format(path, key)
                json_paths.append("{}.{}".format(path, key))

            o['properties'][key] = {
                "type": ["string", "null"]
            }
        elif type(value) == bool:
            if path == '':
                sql_fields += "{} boolean,\n".format(key)
                json_paths.append("{}".format(key))
            else:
                sql_fields += "\"{}.{}\" boolean,\n".format(path, key)
                json_paths.append("{}.{}".format(path, key))

            o['properties'][key] = {
                "type": ["boolean", "null"]
            }
        elif value is None:
            if path == '':
                sql_fields += "{} TO_FILL,\n".format(key)
                json_paths.append("{}".format(key))
            else:
                sql_fields += "\"{}.{}\" TO_FILL,\n".format(path, key)
                json_paths.append("{}.{}".format(path, key))
            o['properties'][key] = {
                "type": ["null", "TO_FILL"]
            }
        elif type(value) == dict:
            if path == '':
                child_o, child_json_paths, child_sql_fields = parse(value, "{}".format(key))
            else:
                child_o, child_json_paths, child_sql_fields = parse(value, "{}.{}".format(path, key))

            o['properties'][key] = child_o
            json_paths = json_paths + child_json_paths
            sql_fields = sql_fields + child_sql_fields

    return o, json_paths, sql_fields


vendor = "com.hubspot.snowplow"
version = "1-0-0"
event_name = "email_events_updates"
description = "Hubspot Email Events"

data = json.loads(x)
data, paths, sql_fields = parse(data)
data["$schema"] = "https://s3.amazonaws.com/bee-bi-content/schemas/{vendor}/{event_name}/jsonschema/{version}#".format(vendor=vendor, event_name=event_name, version=version)

data["description"] = description

data["self"] = {
    "vendor": vendor,
    "name": event_name,
    "format": "jsonschema",
    "version": version
}

jsonpaths = {
    "jsonpaths": [
        "$.schema.vendor",
        "$.schema.name",
        "$.schema.format",
        "$.schema.version",
        "$.hierarchy.rootId",
        "$.hierarchy.rootTstamp",
        "$.hierarchy.refRoot",
        "$.hierarchy.refTree",
        "$.hierarchy.refParent"]
}


sql = """
-- Copyright (c) 2014 Snowplow Analytics Ltd. All rights reserved.
--
-- This program is licensed to you under the Apache License Version 2.0,
-- and you may not use this file except in compliance with the Apache License Version 2.0.
-- You may obtain a copy of the Apache License Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0.
--
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the Apache License Version 2.0 is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Apache License Version 2.0 for the specific language governing permissions and limitations there under.
--
-- Authors:     Gustavo Hwu Lee
--

CREATE TABLE atomic.{table_name}_{event_name}_1
(
  -- Schema of this type
  schema_vendor  varchar(128) encode runlength not null,
  schema_name    varchar(128) encode runlength not null,
  schema_format  varchar(128) encode runlength not null,
  schema_version varchar(128) encode runlength not null,
  -- Parentage of this type
  root_id        char(36) encode raw not null,
  root_tstamp    timestamp encode raw not null,
  ref_root       varchar(255) encode runlength not null,
  ref_tree       varchar(1500) encode runlength not null,
  ref_parent     varchar(255) encode runlength not null,
  -- Properties of this type
  {fields}
    FOREIGN KEY (root_id) REFERENCES atomic.events(event_id)
)
  DISTSTYLE KEY
  -- Optimized join to atomic.events
  DISTKEY
(
  root_id
)
  SORTKEY
(
  root_tstamp
);
""".format(fields = sql_fields, event_name=event_name, table_name=vendor.replace(".","_"))

complete_path = []
for p in paths:
    complete_path.append("$.data." + p)


jsonpaths["jsonpaths"] = jsonpaths["jsonpaths"] + complete_path

with open('{}_1.sql'.format(event_name), 'w') as outfile:
    outfile.write(sql)

with open('{}_1.json'.format(event_name), 'w') as outfile:
    json.dump(jsonpaths, outfile)

with open('{}.json'.format(version), 'w') as outfile:
    json.dump(data, outfile)



